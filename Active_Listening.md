# Active Listening
## Question 1
What are the steps/strategies to do Active Listening? (Minimum 6 points)

- Pay attention to the person talking.
- Don't let your thoughts distract you.
- Wait for them to finish speaking before you respond.
- Use friendly words.
- Show you're listening through your body language.
- Repeat back what they said.

# Reflective Listening
## Question 2
According to Fisher's model, what are the key points of Reflective Listening? (Write in your own words, use simple English)

Fisher's Reflective Listening means repeating what someone says in your own words, asking questions, and paying attention to how they look and sound. And validate them to make them validate feelings, and build trust in a conversation.

## Question 3
What are the obstacles in your listening process?

- Distractions in the environment, like noise or interruptions.


## Question 4
What can you do to improve your listening?

- I can minimize distractions: Create a quiet, interruption-free environment for listening.
- I can be open-minded: Avoid jumping to conclusions and be receptive to different perspectives.
- I can take notes: Jot down key points to help remember and process information.


## Question 5
When do you switch to Passive communication style in your day to day life?
I switch to passive communication when I want to avoid conflict or when I don't want to argue with others.

## Question 6
When do you switch into Aggressive communication styles in your day to day life?
I switch to aggressive communication when someone disrespects me.

## Question 7
When do you switch into Passive Aggressive (sarcasm/gossiping/taunts/silent treatment and others) communication styles in your day to day life?
I might use passive-aggressive communication when someone repeatedly ignores my feelings.

## Question 8
How can you make your communication assertive? You can watch and analyze the videos, then think about what steps you can apply in your own life. (Watch the videos first before answering this question.)
To make communication assertive:
- Express thoughts and feelings clearly and respectfully.
- Use "I" statements to express needs and boundaries effectively.
- Make mutual benefit statements to find solutions that work for both parties.
