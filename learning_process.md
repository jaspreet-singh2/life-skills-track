# Learning Process

## Question 1
**What is the Feynman Technique? Explain in 1 line.**
The Feynman Technique is all about breaking down tricky stuff and explaining it so you really get it and find where you're struggling.

## Question 2
**In the "Learning How to Learn" video, what was the most interesting story or idea for you?**
The speaker's journey from struggling with math and science to becoming a professor of engineering stood out as the most interesting part. It showed the potential for personal growth through effective learning techniques.

## Question 3
**What are active and diffused modes of thinking?**
Active thinking is like when you're fully concentrated on something, while diffused thinking is more like when your mind is relaxed.

## Question 4
**According to the "Learn Anything in 20 hours" video, what are the steps to take when approaching a new topic? Only mention the points.**

Break the skill into parts, gather resources, remove distractions, practice for 20 hours, overcome frustration, have fun while learning.


## Question 5
**What are some of the actions you can take going forward to improve your learning process?**

- Break complex topics into smaller parts for better understanding.
- Eliminate distractions to stay focused during learning.
- Teach what you've learned to reinforce your understanding.


