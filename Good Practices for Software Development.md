### Question 1
New points for me:
- Using Trello or Jira for requirements documentation.
- The idea of using a time tracking app for productivity.

### Question 2
Areas for improvement:
- Enhancing focus through phone management and deep work.
- Improving physical well-being with a structured routine.
