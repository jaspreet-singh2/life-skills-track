**Questions:**

1. What kinds of behavior cause sexual harassment?

   - Sexual harassment can happen when someone touches, talks about sex, sends explicit messages, or makes the environment uncomfortable.

2. What would you do in case you face or witness any incident or repeated incidents of such behavior?

   - If I see or experience this, I would tell someone in charge, help the person being hurt, and make sure they're safe.
